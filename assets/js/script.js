(function(){
	var products,
		filters;
	/**
	 * Filter checkoxes
	 */
	var $checkboxes = document.querySelectorAll('.all-products input[type=checkbox]');

	for(var i=0; i < $checkboxes.length; i++){
		$checkboxes[i].addEventListener('click', function(event){
			var specName = this.getAttribute('name');

			if(this.checked) {
				// If the filter for this specification isn't created yet - do it.
				if(!(filters[specName] && filters[specName].length)){
					filters[specName] = [];
				}

				//	Push values into the chosen filter array
				filters[specName].push(this.value);
				// Change the url hash;
				createQueryHash(filters);
			}

			// When a checkbox is unchecked we need to remove its value from the filters object.
			if(!this.checked) {
				if(filters[specName] && filters[specName].length && (filters[specName].indexOf(this.value) != -1)){
					// Find the checkbox value in the corresponding array inside the filters object.
					var index = filters[specName].indexOf(this.value);
					filters[specName].splice(index, 1);

					if(!filters[specName].length){
						delete filters[specName];
					}
				}
				// Change the url hash;
				createQueryHash(filters);
			}
		});
	}
	/**
	 * Back to main page link
	*/
	var backLink = document.querySelector('.link-back');
	backLink.addEventListener('click',function (event) {
		event.preventDefault();
		window.history.back();
	})

	// Single product page buttons
	var $singleProductPage = document.querySelector('.single-product');

	$singleProductPage.addEventListener('click', function (e) {

		if (hasClass($singleProductPage,'visible')) {

			var clicked = e.target;

			// If the close button or the background are clicked go to the previous page.
			if (hasClass(clicked, 'close') || hasClass(clicked,'overlay')) {
				// Change the url hash with the last used filters.
				createQueryHash(filters);
			}
		}
	});

	readTextFile("products.json", function(data){
		// Write the data into our global variable.
		products = data;

		// Call a function to create HTML for all the products.
		generateAllProductsHTML(products);

		// Manually trigger a hashchange to start the app.
		var hashChange = document.createEvent('HTMLEvents');
		hashChange.initEvent('hashchange', false, true);
		window.dispatchEvent(hashChange);
	});


	window.addEventListener('hashchange', function(){
		render(decodeURI(window.location.hash));
	}, false);

	// Navigation
	function render(url) {
		var temp = url.split('/')[0];
		var pages = document.querySelectorAll('.main-content .page');
		for(var i=0; i < pages.length; i++) {
			removeClass(pages[i], 'visible');
		}

		var	map = {
			// The "Homepage".
			'': function() {
				filters = {};
				for(var i=0; i< $checkboxes.length; i++){
					$checkboxes[i].checked = false;
				}
				renderProductsPage(products);
			},
			// Single Products page.
			'#product': function() {
				var index = url.split('#product/')[1].trim();
				renderSingleProductPage(index, products);
			},
			// Page with filtered products
			'#filter': function() {
				url = url.split('#filter/')[1].trim();
				try {
					filters = JSON.parse(url);
				}catch(err) {
					window.location.hash = '#';
					return;
				}
				renderFilterResults(filters, products);
			}

		};
		// Execute the needed function depending on the url keyword (stored in temp).
		if(map[temp]){
			map[temp]();
		}else{
			renderErrorPage();
		}
	}

	function generateAllProductsHTML(data){

		var list = document.querySelector('.all-products .products-list');

		var theTemplateScript = document.getElementById("products-template").innerText;
		//Compile the template​
		var theTemplate = Handlebars.compile(theTemplateScript);
		list.innerHTML = theTemplate(data);

		var products = list.querySelectorAll('li.product');

		for(var i=0; i<products.length; i++){
			products[i].querySelector('button').addEventListener('click',function(event) {
				event.preventDefault();
				var productIndex = this.parentElement.getAttribute('data-index');
				window.location.hash = 'product/' + productIndex;
			})
		}
	}

	function renderProductsPage(data){

		var page = document.querySelector('.all-products'),
			allProducts = document.querySelectorAll('.all-products .products-list > li');
		// Hide all the products in the products list.
		for(var i=0; i< allProducts.length; i++) {
			addClass(allProducts[i], 'hidden');
			// Iterate over all of the products.
			// If their ID is somewhere in the data object remove the hidden class to reveal them.
			var productIndex = allProducts[i].getAttribute('data-index');
			for(var j=0; j<data.length; j++){
				if(parseInt(productIndex) === data[j].id){
					removeClass(allProducts[i], 'hidden');
				}
			}

		}
		// Show the page itself.
		// (the render function hides all pages so we need to show the one we want).
		addClass(page, 'visible');
	}

	function renderFilterResults(filters, products){
		// This array contains all the possible filter criteria.
		var criteria = ['manufacturer','storage','os','camera'],
			results = [],
			isFiltered = false;
		// Uncheck all the checkboxes.
		// We will be checking them again one by one.
		for(var i=0; i< $checkboxes.length; i++){
			if ($checkboxes[i].checked === true) {
				$checkboxes[i].checked = true;;
			}
		}

		criteria.forEach(function (c) {
			if(filters[c] && filters[c].length){
				if(isFiltered){
					products = results;
					results = [];
				}
				filters[c].forEach(function (filter) {
					// Iterate over the products.
					products.forEach(function (item){
						if(typeof item.specs[c] == 'number'){
							if(item.specs[c] == filter){
								results.push(item);
								isFiltered = true;
							}
						}
						if(typeof item.specs[c] == 'string'){
							if(item.specs[c].toLowerCase().indexOf(filter) != -1){
								results.push(item);
								isFiltered = true;
							}
						}
					});

					if(c && filter){
						var filteredCheckobxes = document.getElementsByName(c);
						for(var i=0; i < filteredCheckobxes.length; i++){
							if(filteredCheckobxes[i].value === filter) filteredCheckobxes[i].checked = true;
						}
					}
				});
			}
		});

		renderProductsPage(results);
	}

	function renderSingleProductPage(index, data){
		var page = document.querySelector('.single-product'),
			container = document.querySelector('.preview-large');
		// Find the wanted product by iterating the data object and searching for the chosen index.
		if(data.length){
			data.forEach(function (item) {
				if(item.id == index){
					// Populate '.preview-large' with the chosen product's data.
					container.querySelector('h3').innerText = item.name;
					container.querySelector('img').setAttribute('src', item.image.large);
					container.querySelector('p').innerText = item.description;
				}
			});
		}
		addClass(page, 'visible');
	}

	function renderErrorPage(){
		var page = document.querySelector('.error');
		addClass(page, 'visible');
	}

	function createQueryHash(filters){
		if(!isEmpty(filters)){
			// Stringify the object via JSON.stringify and write it after the '#filter' keyword.
			window.location.hash = '#filter/' + JSON.stringify(filters);
		}
		else{
			// If it's empty change the hash to '#' (the homepage).
			window.location.hash = '#';
		}

	}
}());

/**
 * Helpers
 */
function hasClass(ele,cls) {
	if(!ele.className) {
		return false;
	}else{
		return !!ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
	}
}

function addClass(ele,cls) {
	if (!hasClass(ele,cls)) ele.className += " "+cls;
}

function removeClass(ele,cls) {
	if (hasClass(ele,cls)) {
		var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		ele.className=ele.className.trim().replace(reg,' ');
	}
}

function readTextFile(file, callback) {
	var rawFile = new XMLHttpRequest();
	rawFile.overrideMimeType("application/json");
	rawFile.open("GET", file, true);
	rawFile.onreadystatechange = function() {
		if (rawFile.readyState === 4 && rawFile.status == "200") {
			callback(JSON.parse(rawFile.responseText));
		}
	}
	rawFile.send(null);
}

function isEmpty(obj) {
	if (obj == null) return true;
	if (obj.length > 0)    return false;
	if (obj.length === 0)  return true;
	if (typeof obj !== "object") return true;
	for (var key in obj) {
		if (hasOwnProperty.call(obj, key)) return false;
	}

	return true;
}